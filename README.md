# HipChat Trac Plugin

Notifies HipChat Rooms when ticket changes

## Install

    git clone https://bitbucket.org/galmeida/hipchatplugin.git
    cd hipchatplugin
    python setup.py bdist_egg
    cp dist/HipChatPlugin-1.0-py2.7.egg $TRAC_ENV/plugins
    
## Config
Create a section called `[hipchat]` in `trac.ini`, this section should have a key `rooms` with a comma separated list of "room/token"'s listening all rooms that should receive notifications for the given trac environment. Exemple:

    [hipchat]
    rooms=RoomName/dgsh436GHhdsghGFdsyghdsk, RoomName2/e53tghdsGfgsd6rhdsgHgds

HipChat room owners are able to create room notification tokens using HipChat web interface:

1. login to hipchat web interface
1. Rooms > *RoomName* > Tokens
1. Label it 'Trac' and click Create
