# HipCHat plugin

from cgi import escape
import urllib2
from json import dumps
from trac.ticket import ITicketChangeListener
from trac.core import Component, implements
from re import split

class HipChatPlugin(Component):
    implements(ITicketChangeListener)

    def send_message(self, msg):
        roomsConfig = self.config.getlist('hipchat', 'rooms')
        rooms = {}
        for roomConfig in roomsConfig:
            tmp = split('/', roomConfig)
            rooms[tmp[0]] = tmp[1]

        self.log.debug(msg)

        for room, token in rooms.items():
            url = "https://api.hipchat.com/v2/room/%s/notification"%room
            data = {"message": msg }
            headers = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer %s'%(token)
            }

            try:
                request = urllib2.Request(url, dumps(data), headers)
                response = urllib2.urlopen(request)
                self.log.debug(response.read())
            except Exception:
                self.log.error("Error posting to HipChat, url: %s"%url)

    def ticket_deleted(self, ticket):
        msg = "<a href=\"%s\">%s - %s</a> deleted"%(self.env.abs_href.ticket(ticket.id), ticket.id, escape(ticket['summary']))
        self.send_message(msg)

    def ticket_created(self, ticket):
        msg = "<a href=\"%s\">%s - %s</a> created"%(self.env.abs_href.ticket(ticket.id), ticket.id, escape(ticket['summary']))
        self.send_message(msg)

    def ticket_changed(self, ticket, comment, author, old_values):
        msg = "<a href=\"%s\">%s - %s</a>"%(self.env.abs_href.ticket(ticket.id), ticket.id, escape(ticket['summary']))
        for key, val in old_values.items():
            msg = "%s, %s: %s &rarr; %s"%(msg, escape(key), escape(val), escape(ticket[key]))

        if(comment):
            msg = "%s, new comment: %s"%(msg, escape(comment))

        msg = "%s by %s"%(msg, escape(author))
        self.send_message(msg)


